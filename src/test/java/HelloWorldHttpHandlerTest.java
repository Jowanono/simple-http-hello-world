import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.*;

class HelloWorldHttpHandlerTest {

    @Test
    void serviceTest() {
        Instant now = Instant.parse("2020-02-02T20:20:02.02Z");
        HelloWorldHttpHandler handler = new HelloWorldHttpHandler(Clock.fixed(now, ZoneId.of("UTC")));

        assertEquals("Hello, world! It is currently 20:20:02.020", handler.getResponseBody());
    }
}