# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

## Dockerisation

```
# Créez une image à partir du Dockerfile
Placez vous dans votre projet et ouvrez un bash
Entrez la commande suivante : `docker build -t hellosrv .`

# Créez un container à partir de l'image tout juste créée
Toujours dans votre bash, rentrez la commande suivante : `docker run -it -d -p 8080:8181 hellosrv`

# Vérifiez que votre container est lancé
Toujours dans votre bash, rentrez la commande suivante : ` docker container ls`
Si votre container est lancé, il apparait.

# Pour vérifier que votre container est opérationnel, rendez-vous sur le port 8080 de votre localhost
via votre navigateur : `http://localhost:8080/hello`
Le message suivant doit d'afficher : `Hello, world! It is currently YOUR_TIMESTAMP`
Félicitations!

# En cas de problème, pensez a consulter les logs
Dans votre bash, rentrez la commande suivante : `docker container ls -a`
Notez le container_ID de votre container
Toujours dans votre bash, rentrez la commande suivante : `docker logs YOUR_CONTAINER_ID`
```